<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departments;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Departments::all();

        return view('departments.index',compact('departments',$departments));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request != null){
            $this->validate($request,[
                'department_name' => 'required',
            ]);
            
           
            $departments = new Departments;
            $departments -> department_name = $request -> input('department_name');
            $departments -> save();
            return 'success';
        }else{
            return 'error';
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Departments::find($id);
        return view('departments.edit',compact('department',$department));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $departments = Departments::where([
            ['id','=',$request->id],
            ])->get()->first();
        if($departments !== null){  
            $this->validate($request,[
                'department_name' => 'required|regex:/^[\pL\s\-]+$/u',
            ]);
            $department = Departments::find($request->id);
            $department -> department_name = $request -> input('department_name');
            $department -> save();
            return "success";
        }else{
            return "error";   
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
