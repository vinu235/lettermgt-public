<?php
 
namespace App\Notifications;
 
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
 
class NewLetter extends Notification
{
   use Queueable;
 
   protected $my_notification; 
 
   public function __construct($msg)
   {
       $this->my_notification = $msg; 
   }
 
   public function via($notifiable)
   {
       return ['mail'];
   }
 
   /*public function toMail($notifiable)
   {
       return (new MailMessage)
                   ->line('Welcome '.$this->my_notification)
                   ->action('Check the New Letter', url('#'))
                   ->line('Thank you for using our application!');
   }*/
   public function toMail($notifiable)
   {   
       return (new MailMessage)
           ->from('admin@lettermgt.com', 'Admin')
           ->subject('New letter in inwards - Letter Management')
           ->markdown('vendor.notifications.email');
   }
   public function toArray($notifiable)
   {
       return [
           //
       ];
   }
}
