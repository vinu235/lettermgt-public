
@if(Auth::user())
<!-- Modal -->
<div class="modal fade" id="departments_create_model" role="dialog">
    <div class="modal-dialog" style="width:40vw;margin-top:10vw;">
        <div class="panel panel-primary">
            <div class="panel-heading">@lang('app.department') @lang('app.create') <a type="button"  data-dismiss="modal" style="float:right;"><i class="fa fa-times" style="font-size:25px;color:red;cursor: pointer;"></i></a></div>
            <div class="panel-body">
                {!! Form::open(['action' => ['DepartmentsController@store'], 'method' => 'POST','id'=>'DepartmentsCreate', 'class' => 'form-horizontal']) !!}
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('department_name') ? ' has-error' : '' }}">
                        <label for="department_name" class="col-md-4 control-label">@lang('app.department') @lang('app.name')</label>
                        <div class="col-md-6">
                            <input id="department_name" type="text" class="form-control" name="department_name" value="{{ old('department_name') }}" required autofocus>
                            @if ($errors->has('department_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('department_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                @lang('app.create')
                            </button>
                            <button type="reset" class="btn btn-danger">
                                @lang('auth.reset')
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#departments_create_model').modal('show').slideToggle(2000);
        $('#add_department').click(function(){
            if($('#department_name').val() == ''){
                $('#department_name').focus();
            }else{
                var department_name = $('#department_name').val();
                var token = $('#token').val();
                $.ajax({
                    type: 'post',
                    data: {'department_name' :department_name , '_token' : token}, 
                    url : "{{ url('/departments') }}",
                    success:function(data){
                        $('#departments_msg').show();
                        $('#departments_msg').attr('class','alert alert-success');
                        $('#departments_msg').html("@lang('app.department') @lang('app.successful')");
                        $('#DepartmentCreate')[0].reset();
                    },
                    error: function(output){
                        $('#departments_msg').show();
                        $('#departments_msg').attr('class','alert alert-danger');
                        $('#departments_msg').html("@lang('app.department') @lang('app.unsuccessful')");
                    }
                });
            } 
        });      
    });
</script>
@endif