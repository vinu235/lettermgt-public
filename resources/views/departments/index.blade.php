@extends('layouts.app')

@section('content')
<br><br><br>
    <h2 style="text-align:center;color:blue;font-weight:bolder;">@lang('app.departments') @lang('app.management')</h2>
    <div class="well">
        <a onclick="$('#departments_view').load('{{url('/departments/create')}}');" style="color:blue;"><i class="fa fa-plus" style="cursor: pointer;font-size:20px;"> @lang('app.new')</i></a>
    </div>
    <div class="clearfix" style="margin:20px;"></div>
    <div style="padding:0px 15px;">
        <table class="table table-condensed table-hover table-bordered" id="departments_table">
            <thead>
                <th>@lang('app.id')</th>
                <th>@lang('app.department') @lang('app.name')</th>
                <th>@lang('app.action')</th>
            </thead>
            <tbody>
                @foreach ($departments as $department)
                <tr>
                    <td style="width:85px;">{{ $department -> id }}</td>
                    <td>{{ $department -> department_name }}</td>
                    <td style="width:85px;">
                        <a onclick="$('#departments_view').load('{{url('/departments/'. $department -> id .'/edit')}}');" title="@lang('app.edit')" style="text-decoration:none;cursor: pointer;"> <i class="far fa-edit" style="font-size:20px;"></i> </a> 
                        <a href="/departments/{{ $department -> id }}" title="@lang('app.view')" style="text-decoration:none;"> <i class="far fa-eye" style="font-size:20px;"></i> </a> 
                        {{-- <a href="/roles/{{ $role -> id }}" title="@lang('app.delete')" style="text-decoration:none;"> <i class="far fa-trash-alt" style="font-size:20px;"></i> </a>  --}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div id="departments_view" ></div>
    <script>
        $(document).ready(function() {
            var departments_table =  $('#departments_table').DataTable({
            dom: 'Bfrtip',
            buttons:[
                        {
                            extend: 'print',
                            exportOptions: {columns: [ 0, 1 ]}
                        },
                        {
                            extend: 'excel',
                            exportOptions: {columns: [ 0, 1 ]}
                        },
                        'colvis'
                    ],
            "columnDefs": [ {
                "targets": 2,
                "orderable": false,
                "searchable": false
                } ]
            });
        });
    </script>
@endsection