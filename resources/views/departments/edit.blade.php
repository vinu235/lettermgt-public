@extends('layouts.app')

@section('content')
<div class="modal fade" id="departments_edit_page" role="dialog">
    <div class="modal-dialog" style="width:40vw;margin-top:10vw;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color:lightblue;color:white;border-radius:5px;text-align:center;margin-bottom:1px;text-shadow: 2px 2px 2px black;">
                <a type="button"  data-dismiss="modal" style="float:right;"><i class="fa fa-times" style="font-size:45px;color:red;cursor: pointer;"></i></a>
                <h3>@lang('app.department') @lang('app.edit')</h3>
            </div>
            <div class="modal-body">
                <div id="departments_msg" hidden style="font-size:16px;padding:5px;"></div>
                {!! Form::open(['action' => ['DepartmentsController@update', $department -> id], 'method' => 'POST']) !!}
                    <input type="hidden" id="token" name="token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="id" class="form-label col-md-3">@lang('app.id') : </label>
                            <div class="col-md-9">
                                <input type="number" class="form-control" id="id" name="id" disabled value="{{ $department -> id }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="name" class="form-label col-md-3">@lang('app.department') : </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="name" name="name" value="{{ $department -> department_name }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-7 col-md-offset-3">
                            <a class="btn btn-success" id="department_update" name="department_update" title="@lang('app.update')"><i class="fa fa-check" style="font-size:25px;margin:0px;cursor:pointer;color:white;text-shadow: 2px 2px 2px black;"></i></a>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>    
</div>
<script>
    $(document).ready(function(){
        $('#departments_edit_page').modal('show').slideToggle(2000);
        
        $('#department_update').click(function(){
            var id = $('#id').val();
            var department_name = $('#department_name').val();
            
            var token = $('#token').val();
            var url1 = "{{ url('/departments/') }}";
            $.ajax({
                type: 'PUT',
                data: {'id': id,'department_name' : department_name,  '_token' : token}, 
                url : url1 + "/edit" + id,
                success:function(data){
                        $('#departments_msg').attr('hidden',false);
                        $('#departments_msg').attr('class','alert alert-success');
                        $('#departments_msg').html("@lang('app.successful')");
                    },
                error: function(output){
                        $('#departments_msg').attr('hidden',false);
                        $('#departments_msg').attr('class','alert alert-danger');
                        $('#departments_msg').html("@lang('app.unsuccessful')");
                    }

            });
        });
        
    });
</script>
@endsection